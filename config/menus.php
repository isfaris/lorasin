<?php
return [
	/*
    |--------------------------------------------------------------------------
    | Menu Position
    |--------------------------------------------------------------------------
    |
    | Determines the themes menu position for front end of theme.
    |
    */
	'primary'   => __('Primary', 'lorasin'),
	'footer'    => __('Footer', 'lorasin'),
	'mobile'    => __('Mobile', 'lorasin'),
];