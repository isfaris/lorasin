<?php

namespace Lorasin\Custom;

/**
 * Custom filter for search
 */
class FilterSearch {

	private $configSearch;

	/**
     * register default hooks and actions for WordPress
     * @return
     */
	public function register()
	{
		// Load config
		$this->configSearch = config_search();

		// Start filter
		add_action( 'pre_get_posts', array( $this, 'handle' ) );
	}

	public function handle( $query ) {
		// Add Pre post for taxonomies
		$taxonomies = apply_filters( 'lorasin_custom_taxonomies', [] );
		if( !is_array($taxonomies)) {
			$taxonomies = [];
		}

		foreach( $taxonomies as $type => $args ) {
			if (empty($query->query_vars['suppress_filters'])) {
				if ( is_tax($type) ) {
					$query->set('post_type', $args['post']);
				}
			}
		}

		if ($query->is_search) {
			$postType = apply_filters( 'lorasin_filter_search_post', $this->configSearch['post_type'] );
	        if( !empty($postType) ) {
	        	$query->set('post_type', $this->configSearch['post_type']);
	        }
	    }
	    return $query;
	}
}