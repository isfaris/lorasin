<?php

namespace Lorasin\Custom;

/**
 * Extras.
 */
class Extras
{
	/**
     * register default hooks and actions for WordPress
     * @return
     */
	public function register()
	{
		add_filter( 'body_class', array( $this, 'body_class' ) );
	}

	public function body_class( $classes )
	{

		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$multiBodyClasess = apply_filters( 'lorasin_multi_author_body_classess', [] );
			if( !empty($multiBodyClasess) ) {
				if( !is_array($multiBodyClasess) ) {
					$classes[] = $multiBodyClasess;
				}

				if( is_array($multiBodyClasess) ) {
					foreach( $multiBodyClasess as $c ) {
						$classes[] = $c;
					}
				}
			}
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$singularBodyClasess = apply_filters( 'lorasin_singular_body_classess', [] );
			if( !empty($singularBodyClasess) ) {
				if( !is_array($singularBodyClasess) ) {
					$classes[] = $singularBodyClasess;
				}

				if( is_array($singularBodyClasess) ) {
					foreach( $singularBodyClasess as $c ) {
						$classes[] = $c;
					}
				}
			}
		}

		$bodyClasses = apply_filters( 'lorasin_body_clasess', [] );
		if( !empty($bodyClasses) ) {
			if( !is_array($bodyClasses) ) {
				$classes[] = $bodyClasses;
			}

			if( is_array($bodyClasses) ) {
				foreach( $bodyClasses as $c ) {
					$classes[] = $c;
				}
			}
		}

		return $classes;
	}
}
