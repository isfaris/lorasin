<?php

namespace Lorasin\Custom;

use Lorasin\Api\Settings;
use Lorasin\Api\Callbacks\SettingsCallback;

/**
 * Admin
 * use it to write your admin related methods by tapping the settings api class.
 */
class Admin
{
	/**
	 * Store a new instance of the Settings API Class
	 * @var class instance
	 */
	public $settings;

	/**
	 * Callback class
	 * @var class instance
	 */
	public $callback;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->settings = new Settings();

		$this->callback = new SettingsCallback();
	}

	/**
     * register default hooks and actions for WordPress
     * @return
     */
	public function register()
	{
		$this->enqueue()->pages()->settings()->sections()->fields()->register_settings();

		$this->enqueue_faq( new Settings() );

		$this->enqueue_code_editor( new Settings() );
	}

	/**
	 * Trigger the register method of the Settings API Class
	 * @return
	 */
	private function register_settings() {
		$this->settings->register();
	}

	/**
	 * Enqueue scripts in specific admin pages
	 * @return $this
	 */
	private function enqueue()
	{
		// Scripts multidimensional array with styles and scripts
		$scripts = array(
			'script' => array( 
				'jquery', 
				'media_uploader',
				get_template_directory_uri() . '/assets/dist/js/admin.js',
				'code_editor',
			),
			'style' => array( 
				get_template_directory_uri() . '/assets/dist/css/admin.css',
				'wp-color-picker'
			)
		);

		// Pages array to where enqueue scripts
		$pages = array( 'toplevel_page_awps' );

		// Enqueue files in Admin area
		$this->settings->admin_enqueue( $scripts, $pages );

		return $this;
	}

	/**
	 * Enqueue only to a specific page different from the main enqueue
	 * @param  Settings $settings 	a new instance of the Settings API class
	 * @return
	 */
	private function enqueue_faq( Settings $settings )
	{
		// Scripts multidimensional array with styles and scripts
		$scripts = array(
			'style' => array( 
				get_template_directory_uri() . '/assets/dist/css/admin.css',
			)
		);

		// Pages array to where enqueue scripts
		$pages = array( 'awps_page_awps_faq' );

		// Enqueue files in Admin area
		$settings->admin_enqueue( $scripts, $pages )->register();
	}

	private function enqueue_code_editor( Settings $settings ) {
		// Scripts multidimensional array with styles and scripts
		$scripts = array(
			'script' => array( 
				'code_editor',
				get_template_directory_uri() . '/assets/dist/js/admin.js',
			)
		);

		// Pages array to where enqueue scripts
		$pages = [ 'theme-settings_page_theme_setting_custom_css', 'theme-settings_page_theme_setting_custom_js' ];

		// Enqueue files in Admin area
		$settings->admin_enqueue( $scripts, $pages )->register();
	}

	/**
	 * Register admin pages and subpages at once
	 * @return $this
	 */
	private function pages()
	{
		$admin_pages = array(
			array(
				'page_title' => __( 'Company Information', 'lorasin' ),
				'menu_title' => __( 'Theme Settings', 'lorasin' ),
				'capability' => 'edit_theme_options',
				'menu_slug' => 'theme_settings',
				'callback' => array( $this->callback, 'admin_company_information' ),
				'icon_url' => get_template_directory_uri() . '/assets/dist/images/paperplane.png',
				'position' => 110,
			)
		);

		$admin_subpages = array(
			[
				'parent_slug' => 'theme_settings',
				'page_title' => __( 'Social Media', 'lorasin' ),
				'menu_title' => __( 'Social Media', 'lorasin' ),
				'capability' => 'edit_theme_options',
				'menu_slug' => 'theme_setting_social_media',
				'callback' => array( $this->callback, 'admin_social_media' )
			],
			[
				'parent_slug' => 'theme_settings',
				'page_title' => __( 'Custom CSS', 'lorasin' ),
				'menu_title' => __( 'Custom CSS', 'lorasin' ),
				'capability' => 'edit_theme_options',
				'menu_slug' => 'theme_setting_custom_css',
				'callback' => array( $this->callback, 'admin_custom_css' )
			],
			[
				'parent_slug' => 'theme_settings',
				'page_title' => __( 'Custom JS', 'lorasin' ),
				'menu_title' => __( 'Custom JS', 'lorasin' ),
				'capability' => 'edit_theme_options',
				'menu_slug' => 'theme_setting_custom_js',
				'callback' => array( $this->callback, 'admin_custom_js' )
			],
			// array(
			// 	'parent_slug' => 'theme_settings',
			// 	'page_title' => 'Awps FAQ',
			// 	'menu_title' => 'FAQ',
			// 	'capability' => 'manage_options',
			// 	'menu_slug' => 'awps_faq',
			// 	'callback' => array( $this->callback, 'admin_faq' )
			// ),
		);

		// Create multiple Admin menu pages and subpages
		$this->settings->addPages( $admin_pages )
			->withSubPage( 'Company Information' )
			->addSubPages( apply_filters( 'lorasin_admin_sub_menus', $admin_subpages ) );

		return $this;
	}

	/**
	 * Register settings in preparation of custom fields
	 * @return $this
	 */
	private function settings()
	{
		// Register settings
		$args = array(
			array(
				'option_group' => 'awps_options_group',
				'option_name' => 'first_name',
				'callback' => array( $this->callback, 'awps_options_group' )
			),
			array(
				'option_group' => 'awps_options_group',
				'option_name' => 'awps_test2'
			),

			/**
			 * Company Information Settings
			 */
			[
				'option_group' => 'lorasin_options_company_group',
				'option_name' => 'company_tagline',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_company_group',
				'option_name' => 'company_phone',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_company_group',
				'option_name' => 'company_fax',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_company_group',
				'option_name' => 'company_email',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_company_group',
				'option_name' => 'company_address',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],

			/**
			 * Social Media Settings
			 */
			[
				'option_group' => 'lorasin_options_sosmed_group',
				'option_name' => 'sosmed_facebook',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_sosmed_group',
				'option_name' => 'sosmed_instagram',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_sosmed_group',
				'option_name' => 'sosmed_twitter',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_sosmed_group',
				'option_name' => 'sosmed_youtube',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],

			/**
			 * Custom CSS Settings
			 */
			[
				'option_group' => 'lorasin_options_css_group',
				'option_name' => 'custom_css_single',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],

			/**
			 * Custom CSS Settings
			 */
			[
				'option_group' => 'lorasin_options_js_group',
				'option_name' => 'custom_js_head',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
			[
				'option_group' => 'lorasin_options_js_group',
				'option_name' => 'custom_js_single',
				'callback' => [ $this->callback, 'awps_options_group' ]
			],
		);

		$this->settings->add_settings( $args );

		return $this;
	}

	/**
	 * Register sections in preparation of custom fields
	 * @return $this
	 */
	private function sections()
	{
		// Register sections
		$args = array(
			// array(
			// 	'id' => 'awps_admin_index',
			// 	'title' => 'Company Information',
			// 	'callback' => array( $this->callback, 'awps_admin_index' ),
			// 	'page' => 'theme_settings'
			// ),
			/**
			 * Company Information
			 */
			[
				'id' => 'theme_setting_company_information',
				'title' => __( 'Company Information', 'lorasin' ),
				'callback' => [ $this->callback, 'theme_setting_company_information'],
				'page' => 'theme_settings'
			],

			/**
			 * Social Media Sections
			 */
			[
				'id' => 'theme_setting_sosmed',
				'title' => __( 'Social Media Information', 'lorasin' ),
				'callback' => [ $this->callback, 'theme_setting_sosmed'],
				'page' => 'theme_setting_social_media'
			],

			/**
			 * Custom CSS Sections
			 */
			[
				'id' => 'theme_setting_custom_css',
				'title' => __( 'Custom CSS', 'lorasin' ),
				'callback' => [ $this->callback, 'theme_setting_custom_css'],
				'page' => 'theme_setting_custom_css'
			],

			/**
			 * Custom CSS Sections
			 */
			[
				'id' => 'theme_setting_custom_js',
				'title' => __( 'Custom JS', 'lorasin' ),
				'callback' => [ $this->callback, 'theme_setting_custom_js'],
				'page' => 'theme_setting_custom_js'
			],
		);

		$this->settings->add_sections( $args );

		return $this;
	}

	/**
	 * Register custom admin fields
	 * @return $this
	 */
	private function fields()
	{
		// Register fields
		$args = array(
			// array(
			// 	'id' => 'first_name',
			// 	'title' => 'First Name',
			// 	'callback' => array( $this->callback, 'first_name' ),
			// 	'page' => 'theme_settings',
			// 	'section' => 'awps_admin_index',
			// 	'args' => array(
			// 		'label_for' => 'first_name',
			// 		'class' => ''
			// 	)
			// ),

			/**
			 * Company Information Settings
			 */
			// [
			// 	'id' => 'company_tagline',
			// 	'title' => __( 'Tagline', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_settings',
			// 	'section' => 'theme_setting_company_information',
			// 	'args' => [
			// 		'option' => 'company_tagline',
			// 		'name' => 'company_tagline',
			// 		'placeholder' => __( 'Tagline', 'lorasin' ),
			// 		'description' => __( 'Your company tagline', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'company_phone',
			// 	'title' => __( 'Phone number', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_settings',
			// 	'section' => 'theme_setting_company_information',
			// 	'args' => [
			// 		'option' => 'company_phone',
			// 		'name' => 'company_phone',
			// 		'placeholder' => __( '+62-8XX-XXXX-XXXX', 'lorasin' ),
			// 		'description' => __( 'Your company official phone line', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'company_fax',
			// 	'title' => __( 'Fax number', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_settings',
			// 	'section' => 'theme_setting_company_information',
			// 	'args' => [
			// 		'option' => 'company_fax',
			// 		'name' => 'company_fax',
			// 		'placeholder' => __( '+62-8XX-XXXX-XXXX', 'lorasin' ),
			// 		'description' => __( 'Your company official fax line', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'company_email',
			// 	'title' => __( 'Email', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_settings',
			// 	'section' => 'theme_setting_company_information',
			// 	'args' => [
			// 		'option' => 'company_email',
			// 		'name' => 'company_email',
			// 		'placeholder' => __( 'hi@company.domain', 'lorasin' ),
			// 		'description' => __( 'Your company official email account', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'company_address',
			// 	'title' => __( 'Address', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render_textarea' ],
			// 	'page' => 'theme_settings',
			// 	'section' => 'theme_setting_company_information',
			// 	'args' => [
			// 		'option' => 'company_address',
			// 		'name' => 'company_address',
			// 		'description' => __( 'Your company address', 'lorasin' ),
			// 	],
			// ],

			/**
			 * Social Media Settings
			 */
			// [
			// 	'id' => 'sosmed_facebook',
			// 	'title' => __( 'Facebook', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_setting_social_media',
			// 	'section' => 'theme_setting_sosmed',
			// 	'args' => [
			// 		'option' => 'sosmed_facebook',
			// 		'name' => 'sosmed_facebook',
			// 		'placeholder' => __( 'Facebook', 'lorasin' ),
			// 		'description' => __( 'Your company official facebook fans page. Example: <code>https://facebook.com/Your-Fans-Page</code>', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'sosmed_instagram',
			// 	'title' => __( 'Instagram', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_setting_social_media',
			// 	'section' => 'theme_setting_sosmed',
			// 	'args' => [
			// 		'option' => 'sosmed_instagram',
			// 		'name' => 'sosmed_instagram',
			// 		'placeholder' => __( 'Instagram', 'lorasin' ),
			// 		'description' => __( 'Your company official instagram account. Example: <code>https://instagram.com/Your-Instagram-Account</code>', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'sosmed_twitter',
			// 	'title' => __( 'Twitter', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_setting_social_media',
			// 	'section' => 'theme_setting_sosmed',
			// 	'args' => [
			// 		'option' => 'sosmed_twitter',
			// 		'name' => 'sosmed_twitter',
			// 		'placeholder' => __( 'Twitter', 'lorasin' ),
			// 		'description' => __( 'Your company official twitter account. Example: <code>https://twitter.com/Your-Twitter-Account</code>', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'sosmed_youtube',
			// 	'title' => __( 'Youtube', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_render' ],
			// 	'page' => 'theme_setting_social_media',
			// 	'section' => 'theme_setting_sosmed',
			// 	'args' => [
			// 		'option' => 'sosmed_youtube',
			// 		'name' => 'sosmed_youtube',
			// 		'placeholder' => __( 'Youtube', 'lorasin' ),
			// 		'description' => __( 'Your company official youtube channel. Example: <code>https://youtube.com/channel/Your-Channel-Id</code>', 'lorasin' ),
			// 	],
			// ],

			/**
			 * Custom CSS Settings
			 */
			// [
			// 	'id' => 'custom_css_single',
			// 	'title' => __( 'Only CSS', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_code_editor' ],
			// 	'page' => 'theme_setting_custom_css',
			// 	'section' => 'theme_setting_custom_css',
			// 	'args' => [
			// 		'id' => 'code_editor_page_css',
			// 		'option' => 'custom_css_single',
			// 		'name' => 'custom_css_single',
			// 		'description' => __( 'Just write css', 'lorasin' ),
			// 	],
			// ],

			/**
			 * Custom JS Settings
			 */
			// [
			// 	'id' => 'custom_js_head',
			// 	'title' => __( 'Head Scripts', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_code_editor' ],
			// 	'page' => 'theme_setting_custom_js',
			// 	'section' => 'theme_setting_custom_js',
			// 	'args' => [
			// 		'id' => 'code_editor_page_head',
			// 		'option' => 'custom_js_head',
			// 		'name' => 'custom_js_head',
			// 		'description' => __( 'Enter scripts and style with the tags such as <code>&lt;script&gt;</code>', 'lorasin' ),
			// 	],
			// ],
			// [
			// 	'id' => 'custom_js_single',
			// 	'title' => __( 'Only Javascript', 'lorasin' ),
			// 	'callback' => [ $this->callback, 'field_code_editor' ],
			// 	'page' => 'theme_setting_custom_js',
			// 	'section' => 'theme_setting_custom_js',
			// 	'args' => [
			// 		'id' => 'code_editor_page_js',
			// 		'option' => 'custom_js_single',
			// 		'name' => 'custom_js_single',
			// 		'description' => __( 'Just write javscript', 'lorasin' ),
			// 	],
			// ],
		);

		// Register fields - Company Information Settings
		$companyArgs = [
			'tagline' => [
				'id' => 'company_tagline',
				'title' => __( 'Tagline', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_settings',
				'section' => 'theme_setting_company_information',
				'args' => [
					'option' => 'company_tagline',
					'name' => 'company_tagline',
					'placeholder' => __( 'Tagline', 'lorasin' ),
					'description' => __( 'Your company tagline', 'lorasin' ),
				],
			],
			'phone' => [
				'id' => 'company_phone',
				'title' => __( 'Phone number', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_settings',
				'section' => 'theme_setting_company_information',
				'args' => [
					'option' => 'company_phone',
					'name' => 'company_phone',
					'placeholder' => __( '+62-8XX-XXXX-XXXX', 'lorasin' ),
					'description' => __( 'Your company official phone line', 'lorasin' ),
				],
			],
			'fax' => [
				'id' => 'company_fax',
				'title' => __( 'Fax number', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_settings',
				'section' => 'theme_setting_company_information',
				'args' => [
					'option' => 'company_fax',
					'name' => 'company_fax',
					'placeholder' => __( '+62-8XX-XXXX-XXXX', 'lorasin' ),
					'description' => __( 'Your company official fax line', 'lorasin' ),
				],
			],
			'email' => [
				'id' => 'company_email',
				'title' => __( 'Email', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_settings',
				'section' => 'theme_setting_company_information',
				'args' => [
					'option' => 'company_email',
					'name' => 'company_email',
					'placeholder' => __( 'hi@company.domain', 'lorasin' ),
					'description' => __( 'Your company official email account', 'lorasin' ),
				],
			],
			'address' => [
				'id' => 'company_address',
				'title' => __( 'Address', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render_textarea' ],
				'page' => 'theme_settings',
				'section' => 'theme_setting_company_information',
				'args' => [
					'option' => 'company_address',
					'name' => 'company_address',
					'description' => __( 'Your company address', 'lorasin' ),
				],
			]
		];
		$companyArgs = apply_filters( 'lorasin_admin_field_company', $companyArgs, $this->callback );

		$socialArgs = [
			'facebook' => [
				'id' => 'sosmed_facebook',
				'title' => __( 'Facebook', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_setting_social_media',
				'section' => 'theme_setting_sosmed',
				'args' => [
					'option' => 'sosmed_facebook',
					'name' => 'sosmed_facebook',
					'placeholder' => __( 'Facebook', 'lorasin' ),
					'description' => __( 'Your company official facebook fans page. Example: <code>https://facebook.com/Your-Fans-Page</code>', 'lorasin' ),
				],
			],
			'instagram' => [
				'id' => 'sosmed_instagram',
				'title' => __( 'Instagram', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_setting_social_media',
				'section' => 'theme_setting_sosmed',
				'args' => [
					'option' => 'sosmed_instagram',
					'name' => 'sosmed_instagram',
					'placeholder' => __( 'Instagram', 'lorasin' ),
					'description' => __( 'Your company official instagram account. Example: <code>https://instagram.com/Your-Instagram-Account</code>', 'lorasin' ),
				],
			],
			'twitter' => [
				'id' => 'sosmed_twitter',
				'title' => __( 'Twitter', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_setting_social_media',
				'section' => 'theme_setting_sosmed',
				'args' => [
					'option' => 'sosmed_twitter',
					'name' => 'sosmed_twitter',
					'placeholder' => __( 'Twitter', 'lorasin' ),
					'description' => __( 'Your company official twitter account. Example: <code>https://twitter.com/Your-Twitter-Account</code>', 'lorasin' ),
				],
			],
			'youtube' => [
				'id' => 'sosmed_youtube',
				'title' => __( 'Youtube', 'lorasin' ),
				'callback' => [ $this->callback, 'field_render' ],
				'page' => 'theme_setting_social_media',
				'section' => 'theme_setting_sosmed',
				'args' => [
					'option' => 'sosmed_youtube',
					'name' => 'sosmed_youtube',
					'placeholder' => __( 'Youtube', 'lorasin' ),
					'description' => __( 'Your company official youtube channel. Example: <code>https://youtube.com/channel/Your-Channel-Id</code>', 'lorasin' ),
				],
			]
		];
		$socialArgs = apply_filters( 'lorasin_admin_field_social', $socialArgs, $this->callback );

		$cssArgs = [
			'css' => [
				'id' => 'custom_css_single',
				'title' => __( 'Only CSS', 'lorasin' ),
				'callback' => [ $this->callback, 'field_code_editor' ],
				'page' => 'theme_setting_custom_css',
				'section' => 'theme_setting_custom_css',
				'args' => [
					'id' => 'code_editor_page_css',
					'option' => 'custom_css_single',
					'name' => 'custom_css_single',
					'description' => __( 'Just write css', 'lorasin' ),
				],
			]
		];
		$cssArgs = apply_filters( 'lorasin_admin_field_css', $cssArgs, $this->callback );

		$jsArgs = [
			'head' => [
				'id' => 'custom_js_head',
				'title' => __( 'Head Scripts', 'lorasin' ),
				'callback' => [ $this->callback, 'field_code_editor' ],
				'page' => 'theme_setting_custom_js',
				'section' => 'theme_setting_custom_js',
				'args' => [
					'id' => 'code_editor_page_head',
					'option' => 'custom_js_head',
					'name' => 'custom_js_head',
					'description' => __( 'Enter scripts and style with the tags such as <code>&lt;script&gt;</code>', 'lorasin' ),
				],
			],

			'single' => [
				'id' => 'custom_js_single',
				'title' => __( 'Only Javascript', 'lorasin' ),
				'callback' => [ $this->callback, 'field_code_editor' ],
				'page' => 'theme_setting_custom_js',
				'section' => 'theme_setting_custom_js',
				'args' => [
					'id' => 'code_editor_page_js',
					'option' => 'custom_js_single',
					'name' => 'custom_js_single',
					'description' => __( 'Just write javscript', 'lorasin' ),
				],
			]
		];
		$jsArgs = apply_filters( 'lorasin_admin_field_js', $jsArgs, $this->callback );
		
		$totalArgs = array_merge( $companyArgs, $socialArgs, $cssArgs, $jsArgs );
		foreach( $totalArgs as $key => $a ) {
			$callback = $a['callback'];
			if( !is_array($callback) ) {
				$a['callback'] = [ $this->callback, $a['callback'] ];
			}
			$args[] = $a;
		}

		$this->settings->add_fields( $args );

		return $this;
	}
}