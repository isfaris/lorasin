<?php

namespace Lorasin\Setup;

class Setup
{
    /**
     * App config variable from app file
     * @var Array
     */
    private $configApp;

    /**
     * Media config variable from media file
     * @var Array
     */
    private $configMedia;

    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register()
    {
        // Init variable
        $this->configApp = config_app();
        $this->configMedia = config_media();;
        
        // Start action
        add_action( 'after_setup_theme', array( $this, 'setup' ) );
        add_action( 'after_setup_theme', array( $this, 'content_width' ), 0);

        // Start filter
        add_filter('excerpt_more', [ $this, 'filter_excerpt_more' ] );
        add_filter('excerpt_length', [ $this, 'filter_excerpt_length' ], 0);
        add_filter('wp_title', [ $this, 'filter_site_title' ], 10, 2);
        add_action('admin_menu', [ $this, 'filter_remove_default_footer' ] );
        add_filter('admin_footer_text', [ $this, 'filter_admin_footer_text' ] );
        add_filter('update_footer', [ $this, 'filter_admin_footer_version' ] );
        add_action('wp_before_admin_bar_render', [ $this, 'filter_remove_wordpress_logo' ], 0);
    }

    public function setup()
    {
        /*
         * You can activate this if you're planning to build a multilingual theme
         */
        // load_theme_textdomain( 'awps', get_template_directory() . '/languages' );

        /*
         * Default Theme Support options better have
         */
        add_theme_support( 'custom-logo' );
        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'customize-selective-refresh-widgets' );
        
        /**
        * Add woocommerce support and woocommerce override
        */
        add_theme_support( 'woocommerce' );

        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        add_theme_support( 'custom-background', apply_filters( 'lorasin_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        ) ) );

        /*
         * Activate Post formats if you need
         */
        add_theme_support( 'post-formats', apply_filters( 'lorasin_post_formats', $this->configApp['post_format'] ) );

        /*
         * Add media sizes
         */
        $this->add_media_size();
    }

    /*
        Define a max content width to allow WordPress to properly resize your images
    */
    public function content_width()
    {
        $GLOBALS[ 'content_width' ] = apply_filters( 'content_width', 1440 );
    }

    /**
     * Add media size descriptions
     */
    private function add_media_size() {
        $sizes = apply_filters( 'lorasin_media_size', $this->configMedia );
        foreach ($sizes as $key => $value) {
            add_image_size($key, $value[0], $value[1], true);
        }
    }

    /**
     * Change read more text
     * @param  STRING $more
     * @return
     */
    public function filter_excerpt_more($more) {
        return apply_filters( 'lorasin_excerpt_more', " ..." );
    }

    /**
     * Change length of excerpt
     * @param  String $length
     * @return
     */
    public function filter_excerpt_length( $length ) {
        return apply_filters( 'lorasin_excerpt_length', $this->configApp['excerpt_length'] );
    }

    /**
     * Filter site title
     * @param  $title
     * @param  $sep
     * @return
     */
    public function filter_site_title( $title, $sep ) {
        if (is_feed()) {
            return $title;
        }

        global $page, $paged;

        // Add the blog name
        $title .= get_bloginfo('name', 'display');

        // Add the blog description for the home/front page
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) {
            $title .= " $sep $site_description";
        }

        // Add a page number if necessary:
        if (($paged >= 2 || $page >= 2) && !is_404()) {
            $title .= " $sep " . sprintf(__('Page %s', '_s'), max($paged, $page));
        }

        if (is_archive() || is_tax()) {
            $title = str_replace("Arsip", "", $title);
            $title = str_replace("Archives", "", $title);
        }

        if (isset($_GET['games'])) {
            switch (get_query_var('games')) {
                case 'quotes':
                    switch (get_query_var('geto')) {
                        case 'gallery':
                            $title = "Gallery Quote";
                            break;
                        case 'create':
                            $title = "Create Quote";
                            break;
                    }
                    break;
            }

            $title .= " $sep $site_description";
        }

        return $title;
    }

    /**
     * Remove wordpress footer
     * @return
     */
    public function filter_remove_default_footer() {
        remove_filter('update_footer', 'core_update_footer');
    }

    public function filter_remove_wordpress_logo() {
        global $wp_admin_bar;

        echo '<script type="text/javascript"> jQuery(document).ready(function(){ ';
        echo  'jQuery("#wp-admin-bar-root-default").prepend(" <li id=\"wlcms_admin_logo\"> <span style=\"float:left;height:28px;line-height:28px;vertical-align:middle;text-align:center;width:28px\"><img src=\"' . assets('images/paperplane.png', false) . '\" width=\"16\" height=\"16\" alt=\"Login\" style=\"height:16px;width:16px;vertical-align:middle\" /> </span> </li> "); ';
        echo '  }); ';
        echo '</script> ';

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
    }

    /**
     * Change created by at left footer dashboard
     * @return
     */
    public function filter_admin_footer_text() {
        $name = apply_filters( 'lorasin_admin_footer_text', $this->configApp['footer_text'] );

        $text = '<span id="footer-thankyou">';
        $text .= "<img src=\"" . assets('images/paperplane.png', false) . "\" width=\"16\" height=\"16\" alt=\"Login\" style=\"height:16px;width:16px;vertical-align:middle;margin-right:10px\" />";
        $text .= '<span>Created by <a target="_blank" href="http://paperplane.id/">Paper Plane</a>.Exclusive for ' . $name . '</span>';
        $text .= '</span>';

        return $text;
    }

    /**
     * Change version at right footer dashboard
     * @return [type] [description]
     */
    public function filter_admin_footer_version() {
        $name = apply_filters( 'lorasin_admin_footer_text', $this->configApp['footer_text'] );
        echo $name;
        return null;
    }
}
