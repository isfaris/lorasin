<?php

namespace Lorasin\Elementor;

class ElementorComponent {
	/**
	 * register custom elementor components
	 * @return [type] [description]
	 */
	public function register()
    {
        add_action( 'elementor/elements/categories_registered', [$this,'elementor_custom_category'] );
        add_action('elementor/init', array($this, 'widgets_registered'));
    }

    public function loadElementFile($file, $name)
    {
        $file = str_replace(".", DIRECTORY_SEPARATOR, $file);
        $file .= ".php";

        $file = __DIR__ . DIRECTORY_SEPARATOR . 'elementor' . DIRECTORY_SEPARATOR . $file;

        if ($file && is_readable($file)) {
            require_once($file);
        }

        if (defined('ELEMENTOR_PATH') && class_exists('Elementor\Widget_Base')) {
            // get our own widgets up and running:
            // copied from widgets-manager.php
            if (class_exists('Elementor\Plugin')) {
                if (is_callable('Elementor\Plugin', 'instance')) {
                    $elementor = \Elementor\Plugin::instance();
                    if (isset($elementor->widgets_manager)) {
                        if (method_exists($elementor->widgets_manager, 'register_widget_type')) {
                            // $template_file = locate_template( $file );
                            $template_file = $file;
                            if ($template_file && is_readable($template_file)) {
                                require_once $template_file;
                                \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new $name());
                            }
                        }
                    }
                }
            }
        }
    }

    public function loadElementClass( $class_name ) {
        // get our own widgets up and running:
        // copied from widgets-manager.php
        if (class_exists('Elementor\Plugin')) {
            if (is_callable('Elementor\Plugin', 'instance')) {
                $elementor = \Elementor\Plugin::instance();
                if (isset($elementor->widgets_manager)) {
                    if (method_exists($elementor->widgets_manager, 'register_widget_type')) {
                        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new $class_name());
                    }
                }
            }
        }
    }

    public function elementor_custom_category( $elements_manager ) {
        $elements_manager->add_category(
            'theme',
            [
                'title' => __( 'Theme', 'plugin-name' ),
                'icon' => 'fa fa-plug',
            ]
        );
    }

    public function widgets_registered()
    {
        // $this->loadElement('jumbotron-title', 'Elementor_oEmbed_Widget');
        // $this->loadElement('button', 'Elementor_Theme_Button_Widget');
        // $this->loadElement('title', 'Elementor_Theme_Title_Widget');
        do_action( 'lorasion_elementor_widgets', $this );
    }
}