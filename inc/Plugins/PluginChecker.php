<?php

namespace Lorasin\Plugins;

class PluginChecker {
	/**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
    	add_action('tgmpa_register', [ $this, 'register_plugins' ] );
    }

    /**
     * Checks the existence of Titan Framework and prompts the display of a notice.
     *
     * @since 1.6
     */
    public function register_plugins() {
    	/*
	     * Array of plugin arrays. Required keys are name and slug.
	     * If the source is NOT from the .org repo, then source is also required.
	     */
    	$plugins = [
    		[
	            'name' => 'Elementor',
	            'slug' => 'elementor',
	            'required' => true,
	        ],
	        [
	            'name' => 'W3 Total Cache',
	            'slug' => 'w3-total-cache',
	            'required' => false,
	        ],
	        [
	            'name' => 'Yoast SEO',
	            'slug' => 'wordpress-seo',
	            'required' => false,
	        ],
	        [
	            'name' => 'Google Analytics Dashboard',
	            'slug' => 'google-analytics-dashboard-for-wp',
	            'required' => false,
	        ],
	        [
	            'name' => 'WP Smush',
	            'slug' => 'wp-smushit',
	            'required' => false,
	        ],
	        [
	            'name' => 'Broken Link Checker',
	            'slug' => 'broken-link-checker',
	            'required' => false,
	        ],
	        [
	            'name' => 'Google XML Sitemaps',
	            'slug' => 'google-sitemap-generator',
	            'required' => false,
	        ],
    	];

    	$config = [
	        'id' => 'lorasin',                     // Unique ID for hashing notices for multiple instances of TGMPA.
	        'default_path' => '',                       // Default absolute path to bundled plugins.
	        'menu'         => 'tgmpa-install-plugins',  // Menu slug.
	        'parent_slug'  => 'themes.php',            // Parent menu slug.
	        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
	        'has_notices'  => true,                    // Show admin notices or not.
	        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
	        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
	        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
	        'message'      => '',                      // Message to output right before the plugins table.
	    ];

	    tgmpa($plugins, $config);
    }
}